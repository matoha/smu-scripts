"""This module implements a driver for the AL1000 syringe pump from
World Precision Instruments

Based on PyExpLabSys, https://github.com/CINF/PyExpLabSys, licensed under GPL
Heavily modified by Martin Holicky to improve the functionality

Manufacturer's documentation is available at https://www.wpiinc.com/media/wysiwyg/pdf/Aladdin-IM.pdf
"""

import serial
import time
import re


class SyringePump:
    """Driver for the AL1000 syringe pump"""

    # Conversion from µl/mL // min/hour
    unit_multipliers = {"UH": 1000, "MH": 60, "MM": 1, "UM": 60000}
    
    def __init__(self, port="/dev/ttyUSB0", baudrate=19200, address=0, syringe_diameter=15):
        """
        Create the driver object

        :param port: serial port, typically /dev/something (Mac, Linux) or COMxx (Windows)
        :param baudrate: baudrate of the RS232 comms
        :param address: pump address for multi-pump networks (see the pump manual for programming it)
        """
        if isinstance(port, str):
            self.serial = serial.Serial(
                port=port, baudrate=baudrate, timeout=1,
                parity=serial.PARITY_NONE,
                bytesize=serial.EIGHTBITS,
                stopbits=serial.STOPBITS_ONE,
            )
        else:
            self.serial = port
        self.address = address
        self.diameter = syringe_diameter
        self._send_command(f"DIA{syringe_diameter}")
        self.clear_vol_disp()
        #print(self._send_command("FUN"))

    def _send_command(self, command: str) -> str:
        """
        Transmit a command to the pump. See the pump manual for a list of possible commands.

        :param command: raw command
        :return: pump's response
        """
        self.serial.flush()
        formatted_command = f"{self.address:02d}" + command + "\r"
        #print(formatted_command)
        self.serial.write(formatted_command.encode("ascii"))
        time.sleep(0.1)
        waiting = self.serial.inWaiting()
        reply = self.serial.read(waiting).decode("ascii")
        #print(reply)
        return reply[4:-1]  # The 4 strips its address and other rubbish

    def _send_command_no_resp(self, command: str):
        """
        Transmit a command to the pump. See the pump manual for a list of possible commands.

        :param command: raw command
        :return: pump's response
        """
        self.serial.flush()
        formatted_command = f"{self.address:02d}" + command + "\r"
        #print(formatted_command)
        self.serial.write(formatted_command.encode("ascii"))
        return True

    def get_status(self) -> str:
        """
        Retrieve the current state of the pump

        :return: pump's response (I=infusing, W=withdrawing, S=stopped, P=paused, T=pause phase, U=wait for trigger)
        """
        formatted_command = f"{self.address:02d}VER\r"  # Ask for the firmware version, there is no specific status cmd
        self.serial.write(formatted_command.encode("ascii"))
        time.sleep(0.5)
        waiting = self.serial.inWaiting()
        reply = self.serial.read(waiting).decode("ascii")
        return reply[3:4] # The status letter appears as the 3rd character in the response

    def get_firmware_version(self) -> str:
        """Retrieves the firmware version
        
        :return: The firmware version string
        """
        return self._send_command("VER")

    def get_rate(self) -> float:
        """Retrieves the pumping rate
        
        :return: The pumping rate in mL/h
        """
        rate_str = self._send_command("RAT")
        # Typical response "70.00UM" (70 uL/min) -- extract the value and units using re
        matches = re.match(r"(\d{1,5}.\d{0,5})(\w\w)", rate_str)
        value = matches[1]
        units = matches[2]
        return float(value) * self.unit_multipliers[units]

    def set_rate(self, ml_h: float):
        """Sets the pump rate.

        :param ml_h: Rate in mL/h
        """
        self._send_command(f"FUNRAT")
        return "rate: " + self._send_command(f"RAT{ml_h:.3f}MM")

    def set_vol(self, ml: float):
        """Sets the volume to be dispensed.
        
        :param ml: Volume to be pumped in mL
        """
        return self._send_command(f"VOL{ml:.3f}")

    def get_vol_disp(self):
        """Retrieves the dispensed volume since last reset.

        Returns:
            The dispensed volume
        """
        return self._send_command("DIS")
    
    def clear_vol_disp(self):
        """Clear pumped volume.
        """
        self._send_command("CLDINF")
        self._send_command("CLDWDR")

    def start(self):
        return self._send_command_no_resp("RUN")

    def stop(self):
        return self._send_command("STP")

    def get_direction(self) -> str:
        """Retrieve the current pumping direction"""
        return self._send_command("DIR")

    def set_direction(self, direction):
        """Set the pumping direction
        
        :param direction: "INF" (infuse), "WDR" (withdraw), "REV" (reverse)
        """
        if direction == "INF":
            return self._send_command("DIRINF")
        elif direction == "WDR":
            return self._send_command("DIRWDR")
        elif direction == "REV":
            return self._send_command("DIRREV")
        else:
            raise RuntimeError("Invalid direction, expected one of INF WDR REV")

    def retract(self):
        """Fully retracts the pump. Needs to be stopped manually.
        """
        self.set_direction("WDR")
        self.set_vol(9999)
        self.set_rate(34.38)
        self.start()

    def wait_for_finished(self, polling_interval: float = 0.5):
        """Wait for the pump to stop moving (this is a blocking call)

        :param polling_interval: poll the pump in this interval
        """
        while True:
            print(self.get_vol_disp())
            print(self.get_status())
            if self.get_status() == "S":
                return
            time.sleep(polling_interval)


if __name__ == "__main__" :
    pump = SyringePump("/dev/tty.usbserial-FT50R4DL", address=0)
    print(pump.get_firmware_version())
    print(pump.get_rate())

