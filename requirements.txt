jupyterlab~=3.0.14
numpy~=1.20.2
matplotlib~=3.4.1
pandas
ipywidgets
ipympl
pyvisa
pyserial~=3.5
