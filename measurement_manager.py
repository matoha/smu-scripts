from syringe_pump import SyringePump
import serial#


class BiosensingManager:
    analyte_concentration = 1
    pumping_rate = 1

    def __init__(self, pump_serial_port):
        self.serial = serial.Serial(
            port=pump_serial_port, baudrate=19200, timeout=1,
            parity=serial.PARITY_NONE,
            bytesize=serial.EIGHTBITS,
            stopbits=serial.STOPBITS_ONE,
        )
        self.buffer_pump = SyringePump(self.serial, address=0, syringe_diameter=28)
        self.analyte_pump = SyringePump(self.serial, address=1, syringe_diameter=28)

    def flush_system(self, requested_concentration: float, total_volume: float):
        flow_ratio = 0  # Ratio of the analyte to the buffer (1 = pure analyte)

        if requested_concentration > self.analyte_concentration:
            print("ERROR: Requested analyte concentration is higher than the prepared, flushing with pure analyte.")
            return
        elif requested_concentration < 0:
            print("ERROR: Analyte concentration cannot be negative, flushing with pure buffer.")
            return
        else:
            flow_ratio = requested_concentration / self.analyte_concentration

        self.buffer_pump.stop()
        self.analyte_pump.stop()

        self.analyte_pump.set_direction("INF")
        self.analyte_pump.set_vol(total_volume * flow_ratio)
        self.analyte_pump.set_rate(self.pumping_rate * flow_ratio)
        self.buffer_pump.set_direction("INF")
        self.buffer_pump.set_vol(total_volume * (1-flow_ratio))
        self.buffer_pump.set_rate(self.pumping_rate * (1-flow_ratio))

        if flow_ratio < 1:
            self.buffer_pump.start()
        if flow_ratio > 0:
            self.analyte_pump.start()
        if flow_ratio < 1:
            self.buffer_pump.wait_for_finished()
            print("BUFFER PUMPING DONE")
        if flow_ratio > 0:
            #self.analyte_pump.wait_for_finished()
            print("ANALYTE PUMPING DONE")


if __name__ == "__main__":
    man = BiosensingManager("COM4")
    #man.flush_system(0.50, 0.1)
    #man.flush_system(0.5, 0.5)
    #man.flush_system(1, 0.5)
