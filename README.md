# SMU scripts

Scripts and Jupyter notebooks for acquiring IV curves and transistor curves with the Keysight U2722A SMU.

Created as part of my PhD at Imperial College.

### Installation instructions

Due to Agilent driver limitations, the scripts below only work on Windows (tested with Windows 10).

1. Install Python (3.5-3.9), following the [official instructions](https://www.python.org/downloads/).
1. Install Keysight IO Libraries from the [Keysight website](https://www.keysight.com/gb/en/lib/software-detail/computer-software/io-libraries-suite-downloads-2175637.html). 
1. Install Agilent Measurement Manager for U2722A from the Keysight [website](https://www.keysight.com/gb/en/lib/software-detail/computer-software/measurement-manager-amm-883008.html) -- this is compulsory as it provides the required drivers for the machine!
1. Download this repository (git clone or as .zip file).
1. Open a command line (Windows key+R then cmd) and go to the folder you have unpacked using the command `cd`, e.g. `cd Downloads\smu-scripts`.
1. Create a Python virtual environment `python -m venv venv` and activate it `venv\Scripts\activate.bat`. For help see [here](https://docs.python.org/3/library/venv.html).
1. Install the required packages `pip install -r requirements.txt`
1. Get the development copy of python-ivi from https://github.com/python-ivi/python-ivi and unpack it into this folder.
1. Install Python-IVI with `cd python-ivi-master` and `python setup.py install`
1. Start Jupyter Lab `jupyter-lab` and open the required notebook.

### Usage instructions

The scripts are written as Jupyter notebooks for easy modifications and easy access to GUI elements. You can find more information about Jupyter [here](https://jupyter.org).

### License

You are free to use this code for whatever you want but must acknowledge its use in published works.
And buy Martin a coffee.
